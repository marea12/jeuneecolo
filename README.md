# ON Y VA

************************************************

Dans votre terminal : 

` git clone git@gitlab.com:marea12/jeune-ecolo.git `

Puis :

` npm install`

Créer un fichier .env dans votre dossier du projet avec à l'interieur :

` DB_HOST=localhost `

` DB_USER=Votre_User_MySql `

` DB_PASS=Votre_mdp_MySql `

Il vous faut la base de donnée "blog". Veuillez la créer puis importer "blog.sql" :

` CREATE DATABASE jeuneecolo; `

` USE jeuneecolo; `

` SOURCE jeuneecolo.sql; `


Pour lancer le serveur :

` npm run start `

Si erreur essayez de relancer votre MySql :

` sudo /etc/init.d/mysql stop `

Puis :

` sudo /etc/init.d/mysql start `


Connectez vous via votre navigateur sur localhost:3000/



************************************************

### [Trello](https://trello.com/b/x00IvBj7/eval-back)

