const bodyParser = require('body-parser');
const express = require('express');
const mysql = require('mysql');
const app = express();

app.use(bodyParser.urlencoded({ extended: false })) 
app.use(bodyParser.json());

require('dotenv').config();

const connection = mysql.createConnection({
	host : process.env.DB_HOST,
	user : process.env.DB_USER,
	password : process.env.DB_PASSWORD,
	database : process.env.DB_DATABASE 
})

connection.connect((err) => {
	if(err){
		console.log(err);
		return;
	}
	console.log('ça marche !')
})

// set view-engine
app.set('view engine', 'pug');

app.use(express.static(`${__dirname}/static`))

app.get('/', (req, res) => {
	connection.query(`SELECT * FROM posts;`, (err, result, fields) => {
		if(err){
			console.log(err)
			return
		}
		// console.log(result);
		res.render('post/index', {results: result})
	})
})
app.get('/post/create', (req, res) => res.render('post/create'))

app.post('/post/store', (req, res) => {
	console.log(req.body.title)
	connection.query(`INSERT INTO posts (title, content, image, date) VALUES ('${req.body.title}', '${req.body.content}', '${req.body.image}', '${new Date()}');`, (err, result, fields) => {
			if(err){
				console.log(err)
			}
	});
	res.redirect('/');
})

app.get('/post/:id', (req, res) => {
	connection.query(`SELECT * FROM posts where id = '${req.params.id}';`, (err, result, fields) => {
		if(err){
			console.log(err);
			return
		}

		res.render('post/show',{result: result[0]})
	})
})


app.listen(process.env.PORT, () => console.log(`listen on ${process.env.PORT}`));
var socket = io();